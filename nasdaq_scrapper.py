import requests
from bs4 import BeautifulSoup
import csv 
import re
from datetime import datetime, date, time
from pytz import timezone
import pytz

def nasdaq_scrapper(ticker): 
    url = 'https://www.nasdaq.com/earnings/report/'
    ticker_url = url+ticker
    page = requests.get(ticker_url)
    soup = BeautifulSoup(page.content, 'html.parser')
    para = soup.find(id="two_column_main_content_reportdata")
    # print(para.get_text())
    return para

sp500 = {}

with open('sp500.csv', mode='r') as infile:
    reader = csv.reader(infile)
    for rows in reader:
        ticker = rows[0]
        name = rows[1]
        industry = rows[2]
        sp500[ticker] = {   'Name': name,
                            'Industry': industry}


i = 1 
with open('sp500.csv', mode='w') as outfile:
    writer = csv.writer(outfile)
    for ticker in sp500.keys():
        sp500[ticker]['Earnings Date'] = {'Start Date': None,
                                        'Start Time': None,
                                        'End Date': None,
                                        'End Time': None}
        sp500[ticker]['NASDAQ Report Paragraph'] = None
        # try: 
        para = re.findall(r'\>(.*?)\<', str(nasdaq_scrapper(ticker)))[0]
        sp500[ticker]['NASDAQ Report Paragraph'] = para
        if 'before' in para:
            date = re.search(r'on (.*?) before', para).group(1)
            date = date.replace(' ', '')
            start_date = datetime.strptime(date, '%m/%d/%Y')
            end_date = start_date
            start_time = time(7, 30, tzinfo=pytz.timezone('America/New_York'))
            end_time = time(9, 30, tzinfo=pytz.timezone('America/New_York'))
            start_datetime = datetime.combine(start_date, start_time)

        if 'after' in para:
            date = re.search(r'on (.*?) after', para).group(1)
            date = date.replace(' ', '')
            start_date = datetime.strptime(date, '%m/%d/%Y')
            end_date = start_date
            start_time = time(4, 30, tzinfo=pytz.timezone('Americas/New York'))
            end_time = time(6, 30, tzinfo=pytz.timezone('Americas/New York'))
            start_datetime = datetime.combine(start_date, start_time)

        sp500[ticker]['Earnings Date']['Start Date'] = start_date
        sp500[ticker]['Earnings Date']['Start Time'] = end_date
        sp500[ticker]['Earnings Date']['End Date'] = end_date
        sp500[ticker]['Earnings Date']['End Time'] = end_time
        writer.writerow([ticker, sp500[ticker]['Earnings Date']['Start Date']])
        break
    # except:
        # print('Error: '+sp500[ticker]['Name'])

# for key, value in sp500.items():
#     print(key)
#     print(value)

# with 
#     writer = csv.writer(csv_file)
#     for key, value in sp500.values():
#         print(key)
#         print(value)
    # for key in sp500.keys():
        
        # writer.writerow([key, value])



#         # print(sp500[ticker]['Report URL'])
#         page = requests.get(sp500[ticker]['Report URL']) #, allow_redirects=False)
#         soup = BeautifulSoup(page.content, 'html.parser')
#         para = soup.find(id="two_column_main_content_reportdata")
#         print(para.get_text())
#         sp500[ticker]['Report'] = para.get_text()

# print(sp500)
# page = requests.get('https://www.nasdaq.com/earnings/report/googl')
# soup = BeautifulSoup(page.content, 'html.parser')
# # print(soup.prettify())
# # print([type(item) for item in list(soup.children)])
# para = soup.find(id="two_column_main_content_reportdata")
# print(para.get_text())


# def simple_get(url):
#     """
#     Attempts to get the content at `url` by making an HTTP GET request.
#     If the content-type of response is some kind of HTML/XML, return the
#     text content, otherwise return None.
#     """
#     try:
#         with closing(get(url, stream=True)) as resp:
#             if is_good_response(resp):
#                 return resp.content
#             else:
#                 return None

#     except RequestException as e:
#         log_error('Error during requests to {0} : {1}'.format(url, str(e)))
#         return None


# def is_good_response(resp):
#     """
#     Returns True if the response seems to be HTML, False otherwise.
#     """
#     content_type = resp.headers['Content-Type'].lower()
#     return (resp.status_code == 200 
#             and content_type is not None 
#             and content_type.find('html') > -1)


# def log_error(e):
#     """
#     It is always a good idea to log errors. 
#     This function just prints them, but you can
#     make it do anything.
#     """
#     print(e)

# raw_html = simple_get('https://www.nasdaq.com/earnings/report/googl')
