from datetime import datetime
from icalendar import Calendar, Event, vDatetime, vCalAddress, vText
import csv 
import uuid
# document = Document('course_schedule.docx')
# table = document.tables[0]
import pytz

event_id = 0
event_dict = {}

with open('test.csv', mode='r') as infile:
    reader = csv.reader(infile)
    for rows in reader:
        title = rows[0]
        start_date = rows[1].split('/')
        start_time = rows[2].split(':')
        end_date = rows[3].split('/')
        end_time = rows[4].split(':')
        location = rows[5]
        description = rows[6]
        start = datetime(   int(start_date[2]), # Year 
                            int(start_date[0]), # Month
                            int(start_date[1]), # Day
                            int(start_time[0]), # Hour
                            int(start_time[1]))
        # start = vDatetime(start).to_ical()
        end = datetime(int(end_date[2]),  # Year
                       int(end_date[0]),  # Month
                       int(end_date[1]),  # Day
                       int(end_time[0]),
                       int(end_time[0]))
        # end = vDatetime(end).to_ical()

        event_dict[title] = {   'Title':title,
                                'Start':start,
                                'End':end,
                                'Location':location,
                                'Description':description,
                                }

cal = Calendar()
cal.add('prodid', '-//My calendar product//test//')
cal.add('version', '2.0')

for i in event_dict.keys():
    event = Event()
    event.add('uid', uuid.uuid1())
    event.add('dtstamp', datetime.now())
    event.add('summary', event_dict[i]['Title'])
    event.add('dtstart', event_dict[i]['Start'])
    event.add('dtend', event_dict[i]['End'])
    event.add('description', event_dict[i]['Description'])
    event.add('location', event_dict[i]['Location'])
    cal.add_component(event)

f = open('test.ics', 'wb')
f.write(cal.to_ical())
f.close()
